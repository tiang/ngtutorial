'use strict';

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    };
  }]);

DonationApp.filter('nfcurrency', ['$filter', '$locale', function ($filter, $locale) {
        var formats = $locale.NUMBER_FORMATS;
        return function (amount, symbol,mutiplier) {
        	console.log(mutiplier);        
        	if (mutiplier)	
        		amount= amount * mutiplier;
    	 	console.log(amount);    
        	console.log(symbol);

            var value = $filter('currency')(amount, symbol);
            return value;
           // return value.replace(new RegExp('\\' + formats.DECIMAL_SEP + '\\d{2}'), '')
        }
    }])


DonationApp.filter('reverse', function() {
    return function(input, uppercase) {
      input = input || '';
      var out = "";
      for (var i = 0; i < input.length; i++) {
        out = input.charAt(i) + out;
      }
      // conditional based on optional argument
      if (uppercase) {
        out = out.toUpperCase();
      }
      return out;
    };
  })