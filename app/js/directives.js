'use strict';

/* Directives */


angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]);


DonationApp.directive('appEnter', function ($timeout) {
    return {
        link: function (scope, element, attrs, controller) {
            element.bind('keydown keypress', function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.appEnter);
                    });

                    event.preventDefault();
                }
            });
        }
    };
});



DonationApp.directive('myForm', function () {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: 'partials/form.html'

    };
});

DonationApp.directive('capitalizeFirst', function (uppercaseFilter) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {

            var capitalize = function (inputValue) {
                if (inputValue == undefined)
                    return;
                var capitalized = inputValue.charAt(0).toUpperCase() +
                    inputValue.substring(1);
                if (capitalized !== inputValue) {
                    modelCtrl.$setViewValue(capitalized);
                    modelCtrl.$render();

                }
              
                return capitalized;
            }

            modelCtrl.$parsers.push(capitalize);
            capitalize(scope[attrs.ngModel]);
        }
    };
});
