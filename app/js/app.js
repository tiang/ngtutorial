'use strict';


// Declare app level module which depends on filters, and services

var DonationApp = angular.module('myApp', [
  'ngRoute', 'myApp.filters', 'myApp.services', 'myApp.directives', 'myApp.controllers'
])


DonationApp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/donors', {
  	templateUrl: 'partials/donors/index.html', 
  	controller: 'DonationController',
  	resolve: {

  	}
  }).when('/intro', {
  	templateUrl: 'partials/basic.html', 
  	controller: 'BasicController',
  	resolve: {
  		
  	}
  });
  $routeProvider.otherwise({redirectTo: '/'});
}]);
