module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-open');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  grunt.initConfig({
    shell: {
      options : {
        stdout: true
      },
      npm_install: {
        command: 'npm install'
      },
      bower_install: {
        command: './node_modules/.bin/bower install'
      },
      font_awesome_fonts: {
        command: 'cp -R bower_components/components-font-awesome/font app/font'
      }
    },
    cssmin: {
      minify: {
        expand: true,
        cwd: 'RateMyAgent.Web/assets/css/release/',
        src: ['*.css', '!*.min.css'],
        dest: 'RateMyAgent.Web/assets/css/release/',
        ext: '.min.css',
      },
      add_banner: {
        options: {
          banner: '/* My minified css file */'
        },
        files: {
          '': ['']
        }
      }
    },
    htmlmin: {                                     // Task
      dist: {                                      // Target
        options: {                                 // Target options
          removeComments: true,
          collapseWhitespace: true
        },
        files: {                                   // Dictionary of files
          '': '',     // 'destination': 'source'
        }
      },
      dev: {                                       // Another target
        files: {
          '': '',
        }
      }
    },
    less: {
      compile: {
        options: {
          paths: [""],
          compress: true,
          yuicompress: true,
          optimization: 2,
          cleancss: true,
        },
        files: {         
          "/assets/css/release/*.css": "RateMyAgent.Web/assets/less/*.less",
        }
      },
    },
    connect: {
      options: {
        base: 'app/'
      },
      webserver: {
        options: {
          port: 8888,
          keepalive: true
        }
      },
      devserver: {
        options: {
          port: 8888
        }
      },
      testserver: {
        options: {
          port: 9999
        }
      },
      coverage: {
        options: {
          base: 'coverage/',
          port: 5555,
          keepalive: true
        }
      }
    },

    open: {
      devserver: {
        path: 'http://localhost:8888'
      },
      coverage: {
        path: 'http://localhost:5555'
      }
    },

    karma: {
      unit: {
        configFile: '.\\RateMyAgent.Web\\Tests\\config\\karma-unit.conf.js',
        autoWatch: true,
        //singleRun: true
      },
      unit_auto: {
        configFile: '.\\RateMyAgent.Web\\Tests\\config\\karma-unit.conf.js',
        autoWatch: true,
      },
      midway: {
        configFile: './test/karma-midway.conf.js',
        autoWatch: false,
        singleRun: true
      },
      midway_auto: {
        configFile: './test/karma-midway.conf.js'
      },
      e2e: {
        configFile: './test/karma-e2e.conf.js',
        autoWatch: false,
        singleRun: true
      },
      e2e_auto: {
        configFile: './test/karma-e2e.conf.js'
      }
    },

    watch: {
      assets: {
        files: ['RateMyAgent.Web/assets/less/*.less'],
        tasks: ['lessDev']
      }
    },
  });

  grunt.registerTask('test', ['connect:testserver','karma:unit','karma:midway', 'karma:e2e']);
  grunt.registerTask('test:unit', ['karma:unit']);
  grunt.registerTask('test:midway', ['connect:testserver','karma:midway']);
  grunt.registerTask('test:e2e', ['connect:testserver', 'karma:e2e']);

  //keeping these around for legacy use
  grunt.registerTask('autotest', ['autotest:unit']);
  grunt.registerTask('autotest:unit', ['connect:testserver','karma:unit_auto']);
  grunt.registerTask('autotest:midway', ['connect:testserver','karma:midway_auto']);
  grunt.registerTask('autotest:e2e', ['connect:testserver','karma:e2e_auto']);

  //installation-related
  grunt.registerTask('install', ['shell:npm_install','shell:bower_install','shell:font_awesome_fonts']);

  //defaults
  grunt.registerTask('default', ['lessDev','watch']);

  // LESS compiler
  grunt.registerTask('lessDev', ['less:compile', 'cssmin'])

  //server daemon
  grunt.registerTask('serve', ['connect:webserver']);
};
